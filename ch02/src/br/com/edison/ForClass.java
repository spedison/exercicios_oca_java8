package br.com.edison;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by spedison on 29/08/15.
 */
public class ForClass {


    static public void main(String... args) {


        List<String> livrosList = new ArrayList<>();

        livrosList.add("Aprendendo C++");
        livrosList.add("C++ in 21 days");
        livrosList.add("JEE in 21 days");
        livrosList.add("Tutorial de java");


        // Forma mais sofisticada de usar o FOR
        String item = "";
        Iterator<String> a;
        for (a = livrosList.iterator(), item = a.next(); a.hasNext(); item = a.next()) {
            System.out.println("Livro:" + item);
        }
    }

}
