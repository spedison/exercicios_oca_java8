package br.edison.bitutil;

/**
 * File PrintBits.java created by @GrupoAlpha on 28/08/2015 at 21:11 for projetct ch01.
 */
public class PrintBits {
    static public String print(byte a) {

        byte val = 1;
        String out = "";

        for (int i = 0; i < 8; i++) {

            if ((i != 0) && (i % 4 == 0))
                out = "_" + out;

            if ((val & a) != 0) {
                out = "1" + out;
            } else {
                out = "0" + out;
            }

            val <<= 1;
        }
        return out;
    }

    static public String print(short a) {

        short val = 1;
        String out = "";

        for (int i = 0; i < 16; i++) {

            if ((i != 0) && (i % 4 == 0))
                out = out + "_";

            if ((val & a) != 0) {
                out = "1" + out;
            } else {
                out = "0" + out;
            }

            val <<= 1;
        }
        return out;
    }


    static public String print(int a) {

        int val = 1;
        String out = "";

        for (int i = 0; i < 32; i++) {

            if ((i != 0) && (i % 4 == 0))
                out = "_" + out;

            if ((val & a) != 0) {
                out = "1" + out;
            } else {
                out = "0" + out;
            }

            val <<= 1;

        }
        return out;
    }

    static public String print(long a) {

        long val = 1;
        String out = "";

        for (int i = 0; i < 64; i++) {

            if ((i != 0) && (i % 4 == 0))
                out = "_" + out;

            if ((val & a) != 0) {
                out = "1" + out;
            } else {
                out = "0" + out;
            }

            val <<= 1;

        }
        return out;
    }


}
