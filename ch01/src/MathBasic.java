import br.edison.bitutil.PrintBits;

/**
 * File MathBasic.java created by @GrupoAlpha on 28/08/2015 at 08:40 for projetct ch01.
 */
public class MathBasic {


    static public void main(String ... args){
        System.out.println("Início");

        byte a = -10;
        a >>= 1;

        double kk = 10.5f;
        kk++;

        byte bShort = 21;
        if(bShort>>2 == 5)
            System.out.println("Funcionou!!!");
        else
            System.out.println("Erro!!!!!!");

        bShort = -20;
        if(bShort>>2 == -5)
            System.out.println("Funcionou!!!");
        else
            System.out.println("Erro!!!!!!");

        bShort = -21;
        if(bShort>>2 == -5)
            System.out.println("Funcionou!!!");
        else
            System.out.println("Erro!!!!!!");

        bShort = -21;
        if(bShort<<2 == -84)
            System.out.println("Funcionou!!!");
        else
            System.out.println("Erro!!!!!!");

        int kkk = -21;
        System.out.println("Int => " + PrintBits.print(kkk) + " ;Deslocado de 2>>> => " + PrintBits.print(kkk>>>2) );
        System.out.println("Int => " + PrintBits.print(kkk) + " ;Deslocado de 2 >> => " + PrintBits.print(kkk >>2) );
        if(bShort>>>2 == -5)
            System.out.println("Funcionou!!!");
        else
            System.out.println("Erro!!!!!!");

        bShort = 21;
        if(bShort<<2 == 84)
            System.out.println("Funcionou!!!");
        else
            System.out.println("Erro!!!!!!");


        int Long = 10;
        System.out.print(Long);

        // Estouro de inteiro!
        int j = 2000000000; // 2 billion
        int jj =  j + j;  /// ESTOURO, valor negativo diferente de 4Bilhoes.
        long jjj = j + j; /// ESTOURO, valor negativo diferente de 4Bilhoes.
        jjj = (long)j+ (long)j; // FUNCIONA: 4 bilhões, mas tá feio.
        long j2 = j;
        jjj = j2 + j2; // FUNCIONA e está Clean Code.

        // Processo de inversão do sinal do número inteiro.
        int val = -30;
        int bits = val;

        if(val >= 0 ){

            bits = ~val;
            bits++;

        } else
        {
            bits = val - 1;
            bits = ~bits;
        }

        if(bits == -val){
            System.out.println("Funcionou!");
        }
        else
        {
            System.out.println("ERRO !!!!");
        }



        /*short c = a + b;
        c = a - b;
        c = a / b;
        c = a * b;
        c = a % b;
        boolean y = a == b;*/




        System.out.println("Saída -> a = " + a);
    }

}
