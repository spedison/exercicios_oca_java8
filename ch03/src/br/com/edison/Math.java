package br.com.edison;

/**
 * Created by spedison on 09/09/15.
 */
public class Math {
    static public void main(String... args) {

        // float a = 22.0 / 7.0; // Error (Result is Double and not Float.
           float a =  22 / 7; //  math Error. a = 3
        // float a = (float)(22/7); // Math error a=3, because 22/7 int is converted to float, but is later more...:-(
        // float a = (float) 22.0/7.0; // OK! a=3.14
        // floar a = (float) 22/7;     // OK! a=3.14
        System.out.println("Iniciando estudos....");
    }

}
