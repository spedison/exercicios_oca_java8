package br.com.edison;

/**
 * Created by spedison on 10/09/15.
 */
public class ByValueOrReference {


    static public void inc(int a) {
        a++;
    }


    static public void inc(Integer a) {
        a++;
    }

    static public void main(String... args) {

        Integer ref = 0;
        int val = 0;

        inc(ref);
        inc(val);


        System.out.println("Val = " + val + " Ref = " + ref);

    }


    static public void errando(){
        String a;
        a = "qqq"; // Se retirar isso dá erro de compilação em "a"
        System.out.println("" + a);
    }


    public void $1agoraVai(){
        System.out.print("Agora vai...");
    }

    public void instanceOf(){
        System.out.print("Agora vai...");
    }
}
